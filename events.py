import json
import logging

ENTRYEVENTTYPE = 1
ENTRYANSWERTTYPE = 2


logger = logging.getLogger(__name__)

__author__ = 'Asgard'
import data_structures

class EventAbstract:
    def tojson(self):
        pass

    def fromjson(self, jsonstring):
        pass

class EntryEvent(EventAbstract):
    def __init__(self, connection=None):
        if not connection: connection = data_structures.Connection()
        self.connection = connection

    def tojson(self):
        return json.dumps({'type': ENTRYEVENTTYPE, 'connection': self.connection.tojson()})

    def fromjson(self, jsontext):
        jsondict = json.loads(jsontext)
        self.connection.fromjson(jsondict['connection'])

class EntryAnswer(EventAbstract):
    def __init__(self):
        self.list = []

    def tojson(self):
        jsondict = {'type': ENTRYANSWERTTYPE, 'list': [i.tojson() for i in self.list]}
        return json.dumps(jsondict)

    def fromjson(self, jsonstring):
        self.list = []
        for connectionjson in json.loads(jsonstring)['list']:
            connection = data_structures.Connection()
            connection.fromjson(connectionjson)
            self.list.append(connection)

def getEvent(eventjson):
    jsondict = json.loads(eventjson)
    if jsondict['type'] == ENTRYEVENTTYPE:
        entryevent = EntryEvent()
        entryevent.fromjson(eventjson)
        return entryevent
    elif jsondict['type'] == ENTRYANSWERTTYPE:
        entryanswer = EntryAnswer()
        entryanswer.fromjson(eventjson)
        return entryanswer