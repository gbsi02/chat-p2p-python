import json
import logging
import time

__author__ = 'Asgard'
import threading
import socket
import random

logger = logging.getLogger(__name__)

PORT=6002
MESSENGERPORT=6003

class ListManager(threading.Thread):
    def __init__(self, connection):
        super(ListManager, self).__init__()
        self.list = []
        self.connection = connection
        self.connected = False
        self.hasList = False
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(('0.0.0.0', PORT))
        self.server.listen(2)
        self.wantToLeave = []
        self.wantToEnter = []

    def createList(self, connection):
        self.list.append(self.connection)
        self.list.append(connection)
        self.hasList = True

    def run(self):
        if self.hasList:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((self.list[-1].addr, PORT))
            self.connection.addr = client.getsockname()[0]
            client.send(connectionListToJson(self.list))
            client.close()
        else:
            sock, addr = self.server.accept()
            self.connection.addr = sock.getsockname()[0]
            message = sock.recv(1024)
            sock.close()
            self.list = jsonToConnectionList(message)
            pos = (self.list.index(self.connection) + 1) % len(self.list)
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((self.list[pos].addr, PORT))
            client.send(connectionListToJson(self.list))
            client.close()

        self.connected = True

        while True:
            sock, addr = self.server.accept()
            message = sock.recv(1024)
            sock.close()
            self.list = jsonToConnectionList(message)
            pos = (self.list.index(self.connection) + 1) % len(self.list)
            while len(self.wantToEnter) > 0:
                element = self.wantToEnter.pop()
                if element not in self.list:
                    self.list.insert(pos, element)
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((self.list[pos].addr, PORT))
            client.send(connectionListToJson(self.list))
            logger.info('List updated and sent: %s - %s' % (self.list[pos].addr, str([con.addr for con in self.list])))
            client.close()


class Messenger(threading.Thread):
    def __init__(self, listmanager=None):
        super(Messenger, self).__init__()
        self.sentmessages = {}
        self.listmanager = listmanager
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(('0.0.0.0', MESSENGERPORT))
        self.server.listen(1)

    def sendMessage(self, content):
        message = Message(content=content, receivedlist=[], sender=self.listmanager.connection)
        self.receive(message)

    def receive(self, message):
        print 'Message from %s(%s): %s' % (message.sender.nick, message.sender.addr, message.content)
        message.receivedlist.append(self.listmanager.connection)
        tosend = [i for i in self.listmanager.list if i not in message.receivedlist]
        self.sentmessages[message.getid()] = message.receivedlist
        while len(tosend) > 0:
            next = random.choice(tosend)
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((next.addr, MESSENGERPORT))
            client.send(message.tojson())
            tosend.remove(next)
            receivedlistjson = client.recv(1024)
            receivedlist = jsonToConnectionList(receivedlistjson)
            reclist = self.sentmessages[message.getid()]
            reclist.extend([con for con in receivedlist if con not in reclist])
            tosend = [i for i in tosend if i not in receivedlist]
            client.close()

    def run(self):
        while True:
            sock, addr = self.server.accept()
            messagejson = sock.recv(4096)
            message = Message()
            message.fromjson(messagejson)
            if message.getid() in self.sentmessages:
                sock.send(connectionListToJson(self.sentmessages[message.getid()]))
                sock.close()
            else:
                sock.send(connectionListToJson([self.listmanager.connection]))
                sock.close()
                self.receive(message)



class Message:
    def __init__(self, content='', receivedlist=[], sender=None):
        self.content = content
        self.receivedlist = receivedlist
        self.timestamp = time.clock()
        self.sender = sender

    def getid(self):
        return self.sender.addr, self.sender.port, self.timestamp

    def tojson(self):
        jsondict = {'receivedlist': connectionListToJson(self.receivedlist), 'content': json.dumps(self.content), 'timestamp': self.timestamp, 'sender': self.sender.tojson()}
        return json.dumps(jsondict)

    def fromjson(self, jsonstring):
        jsondict = json.loads(jsonstring)
        self.receivedlist = jsonToConnectionList(jsondict['receivedlist'])
        self.content = jsondict['content']
        self.timestamp = jsondict['timestamp']
        self.sender = Connection()
        self.sender.fromjson(jsondict['sender'])

    def __eq__(self, other):
        return self.sender == other.sender and self.timestamp == other.timestamp


class Connection:
    def __init__(self, addr='', port=0, nick=''):
        self.addr = addr
        self.port = port
        self.nick = nick

    def tojson(self):
        return json.dumps({'addr': self.addr, 'port': self.port, 'nick': self.nick})

    def fromjson(self, jsontext):
        jsondict = json.loads(jsontext)
        self.addr = jsondict['addr']
        self.port = jsondict['port']
        self.nick = jsondict['nick']

    def __eq__(self, other):
        return self.addr == other.addr

def jsonToConnectionList(jsonstring):
    list = []
    for connectionjson in json.loads(jsonstring)['list']:
        connection = Connection()
        connection.fromjson(connectionjson)
        list.append(connection)
    return list

def connectionListToJson(list):
    return json.dumps({'list': [i.tojson() for i in list]})
