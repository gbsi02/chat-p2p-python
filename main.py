__author__ = 'Asgard'

import sys

from data_structures import ListManager, Messenger, Connection
from events import *
import socket
import random
import time
import logging
import threading

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)

EVENTPORT = 6001

def usage():
    print '''You must pass either first or connect and an IP. Example:
<myprogram>.py first
or
<myprogram>.py connect <nodeip>'''
    sys.exit(0)

first = False
ip_to_connect = ''

if len(sys.argv) < 2:
    usage()

if sys.argv[1] == 'first':
    first = True
else:
    if len(sys.argv) < 3:
        usage()
    else:
        ip_to_connect = sys.argv[2]
        first = False


class EventManager(threading.Thread):
    def __init__(self, ip_to_connect='', nick=''):
        super(EventManager, self).__init__()
        self.ip_to_connect = ip_to_connect
        self.messenger = Messenger()
        self.connection = Connection(port=EVENTPORT, nick=nick)
        self.listmanager = ListManager(self.connection)
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(('0.0.0.0', EVENTPORT))
        self.server.listen(1)

    def create(self):
        logger.info('%s (%s) listening on port %s' % (socket.gethostname(), socket.gethostbyname(socket.gethostname()), EVENTPORT))
        sock, addr = self.server.accept()
        self.connection.addr = sock.getsockname()[0]
        logger.info('connected to %s (%s)' % (socket.gethostbyaddr(addr[0])[0], addr[0]))
        message = sock.recv(1024)

        reply = EntryAnswer()
        reply.list = []
        sock.send(reply.tojson())

        sock.close()

        e = getEvent(message)
        if isinstance(e, EntryEvent):
            print "Evento de entrada do host %s" % (e.connection.addr)
        else:
            print "Evento errado encontrado."
            sys.exit(0)
        self.listmanager.createList(e.connection)
        self.listmanager.start()

    def connect(self):
        event = EntryEvent(self.connection)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((ip_to_connect, EVENTPORT))
        self.connection.addr = client.getsockname()[0]
        client.send(event.tojson())

        message = client.recv(1024)
        # TODO: adicionar erro se der errado
        reply = getEvent(message)
        tosend = reply.list
        # remove itself and the connector from the list
        tosend = [i for i in tosend if i.addr != self.connection.addr and i.addr != ip_to_connect]

        self.listmanager.start()
        while not self.listmanager.connected and len(tosend) > 0:
            element = random.choice(tosend)
            tosend.remove(element)
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((element.addr, EVENTPORT))
            client.send(event.tojson())
            time.sleep(0.2)

        while not self.listmanager.connected:
            time.sleep(0.2)

    def run(self):
        if not self.ip_to_connect:
            self.create()
        else:
            self.ip_to_connect = ip_to_connect
            self.connect()
        self.messenger.listmanager = self.listmanager
        self.messenger.start()

        listeners = {EntryEvent: self.entryEventListener,
                     EntryAnswer: self.entryAnswerListener}

        while True:
            sock, addr = self.server.accept()
            # chamar o listener do evento
            message = sock.recv(1024)
            event = getEvent(message)
            listeners[event.__class__](event, sock)

    def entryEventListener(self, entryevent, sock):
        logger.info("Evento de entrada do host %s" % entryevent.connection.addr)
        self.listmanager.wantToEnter.append(entryevent.connection)
        reply = EntryAnswer()
        reply.list = self.listmanager.list
        sock.send(reply.tojson())

    def entryAnswerListener(self, entryevent):
        pass

if __name__=='__main__':
    print 'Escolha um apelido'
    nick = raw_input()
    manager = EventManager(ip_to_connect=ip_to_connect, nick=nick)
    manager.start()

    print 'Bem vindo ao chat'
    print 'Digite uma mensagem, ou exit para sair.'

    msg = raw_input()

    while (msg != 'exit'):
        manager.messenger.sendMessage(msg)
        print 'Digite uma mensagem, ou exit para sair.'
        msg = raw_input()
